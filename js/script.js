"use strict";

let menuList = document.querySelector(".tabs");
let textList = document.querySelectorAll(".tabs-content>li");
let menuTabs = document.querySelectorAll(".tabs-title");

menuList.onclick = function (event) {
  menuTabs.forEach((element) => {
    element.classList.remove("active");
  });

  let target = event.target;

  if (target.className === "tabs-title") {
    target.classList.add("active");

    textList.forEach((el) => {
      if (el.className === target.innerHTML) {
        el.style.display = "inline-block";
      } else {
        el.style.display = "none";
      }
    });
  }
};
